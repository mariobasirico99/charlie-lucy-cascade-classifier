import cv2
import os
import csv
# declare the classifiers for charlie and lucy
charlie_cascade = cv2.CascadeClassifier('charlie/data/cascade.xml')
lucy_cascade = cv2.CascadeClassifier('lucy/data/cascade.xml')

def detect_charlie_lucy(img):     

    charlie_img = img.copy()   
    charlie_rect = charlie_cascade.detectMultiScale(charlie_img, scaleFactor = 1.2, minNeighbors = 10)
    lucy_rect = lucy_cascade.detectMultiScale(charlie_img, scaleFactor = 1.2, minNeighbors = 10)
  
    charlie_midpoint = []
    for (x, y, w, h) in charlie_rect:
        cv2.rectangle(charlie_img, (x, y),
                      (x + w, y + h), (0, 0, 255), 2)
        charlie_midpoint = ((x+(x+w))/2, (y+(y+h))/2)

    lucy_midpoint = []
    for (x, y, w, h) in lucy_rect:
        cv2.rectangle(charlie_img, (x, y),
                      (x + w, y + h), (0, 255, 0), 2)
        lucy_midpoint = ((x+(x+w))/2, (y+(y+h))/2)

    return (charlie_img, charlie_midpoint, lucy_midpoint)



directory = "undefined/"
hs = open("detect_charlie_lucy.txt","a")
for filename in os.listdir(directory):

    f_read = os.path.join(directory, filename)
    f_write = os.path.join("detected/", filename)
    img = cv2.imread(f_read)
    result = detect_charlie_lucy(img)

    try:
        if result != None:    
            if len(result) == 3:
                line = "{0} {1} {2} {3} {4}\n"
                line = line.format(filename, result[1][0], result[1][1], result[2][0], result[2][1])
                print(filename)
                print(line)
                cv2.imwrite(f_write, result[0])
                hs.write(line)
    except:
        hs.write(filename + " 0 0\n") 
        print("exception")

hs.close()