<a name="readme-top"></a>

<br />
<div align="center">
  <h3 align="center">Charlie and Lucy - Cascade Classifier</h3>

  <p align="center">
    The goal of this repository is to merge the two classifiers created to locate them together in a single panel.    
</div>



```python
# declare the classifiers
charlie_cascade = cv2.CascadeClassifier('charlie/data/cascade.xml')
lucy_cascade = cv2.CascadeClassifier('lucy/data/cascade.xml')
```
```sh
python detect_charlie_lucy.py
```
Output:
```txt
2017-11-19_0.png 187.5 154.5 58.5 192.5
2015-04-26_2.png 137.5 129.5 37.0 113.0
2019-05-22_2.png 184.0 176.0 47.5 155.5
2011-02-20_2.png 178.5 120.5 119.5 140.5
```

<u><b>In this repositories there are only some sample images for each folder.</b></u>

<p align="right">(<a href="#readme-top">back to top</a>)</p>